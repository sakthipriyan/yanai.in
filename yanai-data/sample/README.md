Home Page
-	home.json

Top Schemes
-	top.50.$duration.json
-	top.type.$duration.json
-	top.amc.$duration.json

Mutual Fund Companies
-	amcs.json

Scheme Types
-	sts.json

Mutual Fund Company
-	amc.$id.json
-	amc.$id.top.$duration.json

Scheme Type
-	st.$id.json
-	st.$id.top.$duration.json

Scheme Type and Mutual Fund Company
-	amcst.$amc.$type.$duration.json

Scheme
-	scheme.$schemeid.json
-	scheme.$schemeid.$year.json
