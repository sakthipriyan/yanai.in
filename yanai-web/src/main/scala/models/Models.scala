package models

case class Amc(id: Int, name: String,  link:String)
case class SchemeType(id: Int, name: String,  link:String)

case class Table(link: Link, schemePerformances: List[SchemePerformance])

case class Link(text: String, href: String)

case class SchemePerformance(
  link: Link,
  oneMonth: Performance,
  threeMonths: Option[Performance] = None,
  sixMonths: Option[Performance] = None,
  oneYear: Option[Performance] = None,
  twoYears: Option[Performance] = None,
  threeYears: Option[Performance] = None,
  fiveYears: Option[Performance] = None)

case class Performance(icon: String, text: String)

case class Navbar()

object Navbar {
  def empty = Navbar()
}

object Past extends Enumeration {
  type Past = Value
  val ONE_MONTH, THREE_MONTHS, SIX_MONTHS, ONE_YEAR, TWO_YEARS, THREE_YEARS, FIX_YEARS, MAX_YEARS = Value
}

object Show extends Enumeration {
  type Show = Value
  val THOUSAND_THEN, PERCENTAGE, THOUSAND_NOW = Value
}

//case class Filter(past:Past, show:Show)
