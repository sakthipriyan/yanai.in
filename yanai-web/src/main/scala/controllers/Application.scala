package controllers

import play.api._
import play.api.mvc._
import play.api.libs.json._
import models.Table
import service.TopSchemes
import models.Amc
import models.SchemeType

class Application extends Controller {

  def home = Action {
    Ok(html.main(html.home()))
  }

  def topSchemes = Action {
    Ok(html.main(html.schemes(activeMenu = "overall", List[Table](TopSchemes.getTable))))
  }

  def topSchemesByType = Action {
    Ok(html.main(html.schemes(activeMenu = "type", List[Table]())))
  }

  def topSchemesByAmc = Action {
    Ok(html.main(html.schemes(activeMenu = "amc", List[Table]())))
  }

  def schemeTypes = Action {
    val list = List(SchemeType(1, "Open Ended Scheme - Balanced", "open-ended-scheme-balanced"))
    Ok(html.main(html.schemetypes(list)))
  }

  def schemeType(id: String) = Action {
    Ok(html.main(html.schemetype()))
  }

  def amcs = Action {
    val list = List(Amc(1, "HDFC Mutual Fund", "hdfc-mutual-fund"), Amc(2, "Tata Mutual Fund", "tata-mutual-fund"))
    Ok(html.main(html.amcs(list)))
  }

  def amc(id: String) = Action {
    Ok(html.main(html.amc()))
  }

  def allSchemes(amc: String, st: String) = Action {
    Ok(html.main(html.schemes(activeMenu = "overall", List[Table](TopSchemes.getTable))))
  }

  def scheme(name: String) = Action {
    Ok(html.main(html.scheme()))
  }
}
