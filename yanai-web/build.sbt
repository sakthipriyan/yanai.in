name := "yanai-api"

version := "1.0.0-SNAPSHOT"

organization := "in.yanai"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

disablePlugins(PlayLayoutPlugin)

PlayKeys.playMonitoredFiles ++= (sourceDirectories in (Compile, TwirlKeys.compileTemplates)).value

scalaVersion := "2.11.6"

libraryDependencies ++= Seq(
  ws,
  "com.couchbase.client" % "java-client" % "2.2.2",
  specs2 % Test
)

routesGenerator := InjectedRoutesGenerator

EclipseKeys.preTasks := Seq(compile in Compile)

pipelineStages := Seq(gzip, digest)
